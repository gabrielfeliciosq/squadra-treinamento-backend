using System;
using Xunit;
using ProjetoNewThinkers.Domain.Validators;
using Astronautas = ProjetoNewThinkers.Domain.Entities.Astronauta;

namespace ProjetoNewThinkers.Tests
{
    public class AstronautaTest
    {
        private AstronautaValidator validator;

        public AstronautaTest()
        {
            validator = new AstronautaValidator();
        }

        [Fact]
        public void ValidarEmailSucesso()
        {
            // Arrange
            var validarAstronauta = new Astronautas
            {
                Id = 5,
                Nome = "Gabriel",
                Email = "gabriel@gmail.com"
            };

            // Assert
            Assert.True(validator.Validate(validarAstronauta).IsValid);
        }

        [Fact]
        public void ValidarEmailFalha()
        {
            // Arrange
            var validarAstronauta = new Astronautas
            {
                Id = 5,
                Nome = "Gabriel",
                Email = "gabrielgmail.com"
            };

            // Assert
            Assert.False(validator.Validate(validarAstronauta).IsValid);
        }
    }
}