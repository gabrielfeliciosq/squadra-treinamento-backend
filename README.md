# Projeto New Thinkers (Backend)

### Tecnologias utilizadas?

* .NET Core Web Api
* Entity Framework (ORM)
* Fluent Validation (Validação de dados)
* Swagger (Documentação)
* Mysql (Banco de dados)

### O que é necessário para executar?

* .NET Core 3.1 SDK

### Como configurar o banco de dados?
 
 Você precisa configurar a connection string dentro do arquivo 'appsettings.json'.
     {
       "ConnectionStrings": {
         "local": "Server=162.241.90.61;Database=gabrielf_treinamento;Uid=gabrielf_treinamento;Pwd=@Squadra2020;"
       },
       "Logging": {
         "LogLevel": {
           "Default": "Information",
           "Microsoft": "Warning",
           "Microsoft.Hosting.Lifetime": "Information"
         }
       },
       "AllowedHosts": "*"
     }

 
### Como executar?

 Caso esteja o CLI do .net core, execute ``` dotnet run ``` na pasta ProjetoNewThinkers.WebApi do projeto.

 ### Como ver a documentação gerada com o Swagger?

 Execute a aplicação com o comando acima e acesse ``` localhost:<PORT>/swagger ```.

