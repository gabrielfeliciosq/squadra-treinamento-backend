using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ProjetoNewThinkers.Repositories.Context;
using ProjetoNewThinkers.Repositories.Interfaces;
using ProjetoNewThinkers.Repositories.Repositories;
using FluentValidation.AspNetCore;
using ProjetoNewThinkers.Domain.Validators;
using FluentValidation;
using Microsoft.OpenApi.Models;
using System.Reflection;
using System.IO;

using Planetas = ProjetoNewThinkers.Domain.Entities.Planeta;
using Astronautas = ProjetoNewThinkers.Domain.Entities.Astronauta;

namespace Application
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Injeta o contexto para ser usado em qualquer ponto da aplica��o, passando a connection string
            // SQLServer
            // services.AddDbContext<SSContext>(options => options.UseSqlServer(Configuration.GetConnectionString("local")));
            // MySQL
            services.AddDbContext<SSContext>(options => options.UseMySql(Configuration.GetConnectionString("local")));

            // Injeta o reposit�rio e o Validator
            services.AddTransient<IAstronautaRepository, AstronautaRepository>();
            services.AddTransient<IValidator<Astronautas>, AstronautaValidator>();
            services.AddTransient<IPlanetaRepository, PlanetaRepository>();
            services.AddTransient<IValidator<Planetas>, PlanetaValidator>();

            // Faz a libera��o do CORS para que n�o tenha problema no navegador
            services.AddCors(Options => Options.AddPolicy("AllowAll", p => p.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader()));

            // Configura��o para n�o retornar os atributos nulos no json, assim econimizando em tr�fego.
            // Logo em seguida configura o retorno de BadRequest com o erro validado pelo Fluent Validador quando receber informa��es inconsistentes do modelo
            services.AddControllers().AddFluentValidation().AddJsonOptions(options => {
                options.JsonSerializerOptions.IgnoreNullValues = true;
            }).ConfigureApiBehaviorOptions(options => {
                options.InvalidModelStateResponseFactory = c =>
                {
                    return new BadRequestObjectResult(new { errorMessage = c.ModelState.Values.Select(c => c.Errors.FirstOrDefault().ErrorMessage) });
                };
            });

            // Registro do Swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                    new OpenApiInfo
                    {
                        Title = "Projeto New Thinkers",
                        Version = "v1",
                        Description = "Projeto New Thinkers",
                     });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c => {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Projeto New Thinkers v1.0");
            });

            app.UseCors("AllowAll");

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
