﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ProjetoNewThinkers.Domain;
using ProjetoNewThinkers.Domain.DTO;
using ProjetoNewThinkers.Repositories.Interfaces;
using Astronautas = ProjetoNewThinkers.Domain.Entities.Astronauta;

namespace Application.Controllers
{
    [ApiController]
    [Route("v1/[controller]")]
    public class AstronautaController : ControllerBase
    {
        /// <summary>
        /// Retorna uma lista de astronautas.
        /// </summary>
        /// <param name="nome"></param>
        /// <param name="email"></param>
        /// <returns>Retorna uma lista de astronautas.</returns>
        /// <response code="200">Retorna um objeto json contendo o número resultados, a página solicitada e a lista de astronautas.</response>
        /// <response code="500">Se houver algum problema interno.</response>          
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        [HttpGet]
        public async Task<IActionResult> Filtrar([FromServices] IAstronautaRepository astronautaRepository,
            [FromQuery(Name = "nome")] string nome,
            [FromQuery(Name = "email")] string email)
        {
            try
            {
                var result = await astronautaRepository.Filtrar(nome, email);

                if (result.Result != null)
                    return StatusCode(200, result);

                var messageResult = new JsonResult(new {errorMessage = Messages.NotResult});

                return StatusCode(200, messageResult);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        /// <summary>
        /// Retorna um astronauta.
        /// </summary>
        /// <param name="id">Id do astronauta</param>
        /// <returns>Retorna um astronauta cujo id corresponde ao que foi informado.</returns>
        /// <response code="200">Retorna um astronauta.</response>
        /// <response code="400">Caso o astronauta não seja encontrado.</response>          
        /// <response code="500">Se houver algum problema interno.</response>     
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [HttpGet("{id}")]
        public async Task<IActionResult> ObterPorId([FromServices] IAstronautaRepository astronautaRepository, int id)
        {
            try
            {
                var sys = await astronautaRepository.ObterPorId(id);

                if (sys != null)
                {
                    return StatusCode(200, sys);
                }

                return BadRequest(new
                    {ErrorMessage = "Não foi possivel encontrar nenhum astronauta com o id informado."});
            }
            catch
            {
                return StatusCode(500);
            }
        }

        /// <summary>
        /// Cadastra um novo astronauta.
        /// </summary>
        /// <remarks>
        /// Exemplo de request:
        /// 
        ///     POST v1/Astrounauta
        ///     {        
        ///       "nome": "Sim Multi",
        ///       "email": "gabriel@squadra.com.br",
        ///       "idade": "23"
        ///     }
        /// </remarks>
        /// <param name="astronauta"></param>
        /// <returns>Retorna uma mensagem informando o resultado.</returns>
        /// <response code="201">Retorna uma mensagem informando que a operação foi realizada com sucesso.</response>
        /// <response code="500">Se houver algum problema interno.</response>          
        [ProducesResponseType(201)]
        [ProducesResponseType(500)]
        [HttpPost]
        public async Task<IActionResult> Salvar([FromServices] IAstronautaRepository astronautaRepository,
            Astronautas astronauta)
        {
            try
            {
                await astronautaRepository.Salvar(astronauta);

                return StatusCode(201, new {successMessage = Messages.SuccessOperation});
            }
            catch
            {
                return StatusCode(500);
            }
        }

        /// <summary>
        /// Altera um astronauta cadastrado.
        /// </summary>
        /// <remarks>
        /// Exemplo de request:
        /// 
        ///     PUT v1/Astrounauta
        ///     { 
        ///       "id": 1,
        ///       "nome": "Sim Multi",
        ///       "email": "gabriel@squadra.com.br",
        ///       "idade": "23
        ///     }
        /// </remarks>"
        /// <param name="astronauta"></param>
        /// <returns>Retorna uma mensagem informando o resultado.</returns>
        /// <response code="200">Retorna uma mensagem informando que a operação foi realizada com sucesso.</response>
        /// <response code="400">Se o parametro informado for inválido</response>     
        /// <response code="500">Se houver algum problema interno.</response>   
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        [HttpPut]
        public async Task<IActionResult> Alterar([FromServices] IAstronautaRepository astronautaRepository,
            Astronautas astronauta)
        {
            try
            {
                if (astronauta.Id == 0)
                {
                    return BadRequest();
                }

                await astronautaRepository.Alterar(astronauta);

                return StatusCode(200, new {successMessage = Messages.SuccessOperation});
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        /// <summary>
        /// Exclui um astronauta.
        /// </summary>
        /// <returns>Retorna uma mensagem informando o resultado.</returns>
        /// <response code="200">Retorna uma mensagem informando que a operação foi realizada com sucesso.</response>
        /// <response code="400">Se o parametro informado for inválido</response>     
        /// <response code="500">Se houver algum problema interno.</response>   
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        [HttpDelete]
        public async Task<IActionResult> Excluir([FromServices] IAstronautaRepository astronautaRepository,
            int id)
        {
            try
            {
                if (id == 0)
                {
                    return BadRequest();
                }

                await astronautaRepository.Excluir(id);

                return StatusCode(200, new {successMessage = Messages.SuccessOperation});
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }
    }
}