﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ProjetoNewThinkers.Domain;
using ProjetoNewThinkers.Domain.DTO;
using ProjetoNewThinkers.Repositories.Interfaces;
using Planetas = ProjetoNewThinkers.Domain.Entities.Planeta;

namespace Application.Controllers
{
    [ApiController]
    [Route("v1/[controller]")]
    public class PlanetaController : ControllerBase
    {
        /// <summary>
        /// Retorna uma lista de planetas.
        /// </summary>
        /// <param name="nome"></param>
        /// <returns>Retorna uma lista de planetas.</returns>
        /// <response code="200">Retorna um objeto json contendo o número resultados, a página solicitada e a lista de planetas.</response>
        /// <response code="500">Se houver algum problema interno.</response>          
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        [HttpGet]
        public async Task<IActionResult> Filtrar([FromServices] IPlanetaRepository planetaRepository,
            [FromQuery(Name = "nome")] string nome)
        {
            try
            {
                var result = await planetaRepository.Filtrar(nome);

                if (result.Result != null)
                    return StatusCode(200, result);

                var messageResult = new JsonResult(new {errorMessage = Messages.NotResult});

                return StatusCode(200, messageResult);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        /// <summary>
        /// Retorna um planeta.
        /// </summary>
        /// <param name="id">Id do planeta</param>
        /// <returns>Retorna um planeta cujo id corresponde ao que foi informado.</returns>
        /// <response code="200">Retorna um planeta.</response>
        /// <response code="400">Caso o planeta não seja encontrado.</response>          
        /// <response code="500">Se houver algum problema interno.</response>     
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPorId([FromServices] IPlanetaRepository planetaRepository, int id)
        {
            try
            {
                var sys = await planetaRepository.ObterPorId(id);

                if (sys != null)
                {
                    return StatusCode(200, sys);
                }

                return BadRequest(new
                    {ErrorMessage = "Não foi possivel encontrar nenhum planeta com o id informado."});
            }
            catch
            {
                return StatusCode(500);
            }
        }

        /// <summary>
        /// Cadastra um novo planeta.
        /// </summary>
        /// <remarks>
        /// Exemplo de request:
        /// 
        ///     POST v1/Planeta
        ///     {        
        ///       "nome": "Terra",
        ///       "distanciaMediaSol": "213123",
        ///       "rotacao": "12312",
        ///       "translacao": "1231",
        ///       "diametro": "231",
        ///       "massa": "231",
        ///       "temperaturaMediaNerficieaSup": "2312",
        ///       "pressaoAtmosferica": "312",
        ///       "luas": "1"
        ///     }
        /// </remarks>
        /// <param name="planeta"></param>
        /// <returns>Retorna uma mensagem informando o resultado.</returns>
        /// <response code="201">Retorna uma mensagem informando que a operação foi realizada com sucesso.</response>
        /// <response code="500">Se houver algum problema interno.</response>          
        [ProducesResponseType(201)]
        [ProducesResponseType(500)]
        [HttpPost]
        public async Task<IActionResult> Salvar([FromServices] IPlanetaRepository planetaRepository,
            Planetas planeta)
        {
            try
            {
                await planetaRepository.Salvar(planeta);

                return StatusCode(201, new {successMessage = Messages.SuccessOperation});
            }
            catch
            {
                return StatusCode(500);
            }
        }

        /// <summary>
        /// Altera um planeta cadastrado.
        /// </summary>
        /// <remarks>
        /// Exemplo de request:
        /// 
        ///     PUT v1/Planeta
        ///     {        
        ///       "id": 1,
        ///       "nome": "Terra",
        ///       "distanciaMediaSol": "213123",
        ///       "rotacao": "12312",
        ///       "translacao": "1231",
        ///       "diametro": "231",
        ///       "massa": "231",
        ///       "temperaturaMediaNerficieaSup": "2312",
        ///       "pressaoAtmosferica": "312",
        ///       "luas": "1"
        ///     }
        /// </remarks>
        /// <param name="planeta"></param>
        /// <returns>Retorna uma mensagem informando o resultado.</returns>
        /// <response code="200">Retorna uma mensagem informando que a operação foi realizada com sucesso.</response>
        /// <response code="400">Se o parametro informado for inválido</response>     
        /// <response code="500">Se houver algum problema interno.</response>   
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        [HttpPut]
        public async Task<IActionResult> Alterar([FromServices] IPlanetaRepository planetaRepository,
            Planetas planeta)
        {
            try
            {
                if (planeta.Id == 0)
                {
                    return BadRequest();
                }

                await planetaRepository.Alterar(planeta);

                return StatusCode(200, new {successMessage = Messages.SuccessOperation});
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }


        /// <summary>
        /// Exclui um planeta.
        /// </summary>
        /// <returns>Retorna uma mensagem informando o resultado.</returns>
        /// <response code="200">Retorna uma mensagem informando que a operação foi realizada com sucesso.</response>
        /// <response code="400">Se o parametro informado for inválido</response>     
        /// <response code="500">Se houver algum problema interno.</response>   
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        [HttpDelete]
        public async Task<IActionResult> Excluir([FromServices] IPlanetaRepository planetaRepository,
            int id)
        {
            //TODO Implementar a logica de exclusão   

            return null;
        }
    }
}