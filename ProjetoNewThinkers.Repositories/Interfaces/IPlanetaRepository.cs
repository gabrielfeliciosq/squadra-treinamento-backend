﻿using ProjetoNewThinkers.Domain.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Planetas = ProjetoNewThinkers.Domain.Entities.Planeta;

namespace ProjetoNewThinkers.Repositories.Interfaces
{
    public interface IPlanetaRepository
    {
        Task Salvar(Planetas planeta);
        Task Alterar(Planetas planeta);
        Task<PlanetaDTO> Filtrar(string nome);
        Task<Planetas> ObterPorId(int id);
    }
}
