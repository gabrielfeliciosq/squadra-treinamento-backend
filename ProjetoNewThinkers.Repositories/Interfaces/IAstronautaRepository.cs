﻿using ProjetoNewThinkers.Domain.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Astronautas = ProjetoNewThinkers.Domain.Entities.Astronauta;

namespace ProjetoNewThinkers.Repositories.Interfaces
{
    public interface IAstronautaRepository
    {
        Task Salvar(Astronautas astronauta);
        Task Alterar(Astronautas astronauta);
        Task Excluir(int id);
        Task<AstronautaDTO> Filtrar(string nome, string email);
        Task<Astronautas> ObterPorId(int id);
    }
}
