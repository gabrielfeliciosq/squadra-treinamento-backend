﻿using ProjetoNewThinkers.Repositories.Context;
using ProjetoNewThinkers.Repositories.Interfaces;
using System;
using System.Linq;
using Planetas = ProjetoNewThinkers.Domain.Entities.Planeta;
using System.Collections.Generic;
using ProjetoNewThinkers.Domain.DTO;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace ProjetoNewThinkers.Repositories.Repositories
{
    // Responsável por fazer as interações com o banco de dados.
    public class PlanetaRepository : IPlanetaRepository
    {
        private readonly SSContext _ctx;

        public PlanetaRepository(SSContext ctx)
        {
            _ctx = ctx;
        }

        public async Task Salvar(Planetas planeta)
        {
            _ctx.planeta.Add(planeta);
            await _ctx.SaveChangesAsync();
        }

        public async Task<Planetas> ObterPorId(int id)
        {
            return await _ctx.planeta.AsNoTracking().FirstOrDefaultAsync(c => c.Id == id);
        }

        public async Task<PlanetaDTO> Filtrar(string nome)
        {
            return new PlanetaDTO()
            {
                Result = await (from c in _ctx.planeta.AsNoTracking()
                    where c.Nome.Contains(nome ?? "")
                    select c).ToListAsync()
            };
        }

        public async Task Alterar(Planetas planeta)
        {
            _ctx.planeta.Update(planeta);
            await _ctx.SaveChangesAsync();
        }

        public async Task Excluir(int id)
        {
            //TODO Implementar Logica de exclusão. 
        }
    }
}