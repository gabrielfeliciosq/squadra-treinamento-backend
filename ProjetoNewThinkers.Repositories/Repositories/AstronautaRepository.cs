﻿using ProjetoNewThinkers.Repositories.Context;
using ProjetoNewThinkers.Repositories.Interfaces;
using System;
using System.Linq;
using Astronautas = ProjetoNewThinkers.Domain.Entities.Astronauta;
using System.Collections.Generic;
using ProjetoNewThinkers.Domain.DTO;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace ProjetoNewThinkers.Repositories.Repositories
{
    // Responsável por fazer as interações com o banco de dados.
    public class AstronautaRepository : IAstronautaRepository
    {
        private readonly SSContext _ctx;

        public AstronautaRepository(SSContext ctx)
        {
            _ctx = ctx;
        }

        public async Task Salvar(Astronautas astronauta)
        {
            _ctx.astronauta.Add(astronauta);
            await _ctx.SaveChangesAsync();
        }

        public async Task<Astronautas> ObterPorId(int id)
        {
            return await _ctx.astronauta.AsNoTracking().FirstOrDefaultAsync(c => c.Id == id);
        }

        public async Task<AstronautaDTO> Filtrar(string nome, string email)
        {
            return new AstronautaDTO()
            {
                Result = await (from c in _ctx.astronauta.AsNoTracking()
                    where c.Nome.Contains(nome ?? "") &&
                          c.Email.Contains(email ?? "") 
                    select c).ToListAsync()
            };
        }

        public async Task Alterar(Astronautas astronauta)
        {
            _ctx.astronauta.Update(astronauta);
            await _ctx.SaveChangesAsync();
        }

        public async Task Excluir(int id)
        {
            var astronautaObj = _ctx.astronauta.AsNoTracking().FirstOrDefaultAsync(c => c.Id == id);

            _ctx.astronauta.Remove(await astronautaObj);
            await _ctx.SaveChangesAsync();
        }
    }
}