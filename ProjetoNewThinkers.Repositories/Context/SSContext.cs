﻿using Microsoft.EntityFrameworkCore;
using Astronautas = ProjetoNewThinkers.Domain.Entities.Astronauta;
using Planetas = ProjetoNewThinkers.Domain.Entities.Planeta;

namespace ProjetoNewThinkers.Repositories.Context
{
    public class SSContext : DbContext
    {
        // Injeta as informações do banco para a DbContext
        public SSContext(DbContextOptions<SSContext> options) : base(options)
        {
        }

        public DbSet<Astronautas> astronauta { get; set; }
        public DbSet<Planetas> planeta { get; set; }
    }
}