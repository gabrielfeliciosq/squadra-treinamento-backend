﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using Astronautas = ProjetoNewThinkers.Domain.Entities.Astronauta;

namespace ProjetoNewThinkers.Domain.Validators
{
    // Usando o Fluent Validation para realizar as validações, aqui são definidas regras para algumas propriedades do modelo que sempre são validadas ao serem recebidas na requisição
    public class AstronautaValidator : AbstractValidator<Astronautas>
    {
        public AstronautaValidator()
        {
            RuleFor(c => c.Email)
                .Matches(
                    "^(([^<>()[\\]\\.,;:\\s@\"]+(\\.[^<>()[\\]\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$")
                .WithMessage(Messages.InvalidEmail);

            RuleFor(c => c.Nome)
                .NotEmpty().WithMessage(Messages.InvalidData)
                .MaximumLength(100).WithMessage("Número máximo de 100 caracteres");

            RuleFor(c => c.Idade)
                .NotEmpty().WithMessage(Messages.InvalidData);
        }
    }
}