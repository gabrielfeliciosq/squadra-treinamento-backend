﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using Planetas = ProjetoNewThinkers.Domain.Entities.Planeta;

namespace ProjetoNewThinkers.Domain.Validators
{
    // Usando o Fluent Validation para realizar as validações, aqui são definidas regras para algumas propriedades do modelo que sempre são validadas ao serem recebidas na requisição
    public class PlanetaValidator : AbstractValidator<Planetas>
    {
        public PlanetaValidator()
        {
            RuleFor(c => c.Nome)
                .NotEmpty().WithMessage(Messages.InvalidData)
                .MaximumLength(100).WithMessage("Número máximo de 100 caracteres");
        }
    }
}