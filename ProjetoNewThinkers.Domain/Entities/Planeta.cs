﻿using System;

namespace ProjetoNewThinkers.Domain.Entities
{
    public class Planeta
    {
        public int Id { get; set; }
        public string Nome { get; set; }
    }
}