﻿using System;
using System.Numerics;

namespace ProjetoNewThinkers.Domain.Entities
{
    public class Astronauta
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public long Idade { get; set; }
    }
}