﻿using System;
using System.Collections.Generic;
using System.Text;
using Planetas = ProjetoNewThinkers.Domain.Entities.Planeta;

namespace ProjetoNewThinkers.Domain.DTO
{
    // DTO - Data Transfer Object - Objeto de transferência de dados
    public class PlanetaDTO
    {
        public IEnumerable<Planetas> Result { get; set; }
    }
}