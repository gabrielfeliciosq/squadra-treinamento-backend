﻿using System;
using System.Collections.Generic;
using System.Text;
using Astronautas = ProjetoNewThinkers.Domain.Entities.Astronauta;

namespace ProjetoNewThinkers.Domain.DTO
{
    // DTO - Data Transfer Object - Objeto de transferência de dados
    public class AstronautaDTO
    {
        public IEnumerable<Astronautas> Result { get; set; }
    }
}